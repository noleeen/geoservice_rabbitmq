package notify

import (
	"log"
	"notify/config"
	"notify/internal/entities/notifyEntity"
	"notify/internal/errors"
	"notify/internal/provider"
)

type Notify struct {
	email provider.Sender
	conf  config.Email
}

func NewNotify(conf config.Email, email provider.Sender) Notifier {
	return &Notify{
		email: email,
		conf:  conf,
	}
}

func (n *Notify) Push(in notifyEntity.PushIn) notifyEntity.PushOut {
	err := n.email.Send(provider.SendIn{
		To:    in.Identifier,
		From:  n.conf.From,
		Title: in.Title,
		Type:  "text/plain",
		Data:  in.Data,
	})
	if err != nil {
		log.Println("|Notify.Push| error", err)
		return notifyEntity.PushOut{ErrorCode: errors.InternalError}
	}

	log.Printf("На почту [%s] должно быть отправлено сообщение от [%s]: [%s]", in.Identifier, n.conf.From, in.Data)
	return notifyEntity.PushOut{ErrorCode: errors.NoError}
}
