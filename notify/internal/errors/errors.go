package errors

const (
	NoError = iota
	InternalError
	MessageQueueConnError
	SubscribeError
)
