package mq

import (
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/ptflp/gopubsub/queue"
	"gitlab.com/ptflp/gopubsub/rabbitmq"
	"log"
	"notify/config"
)

func NewRabbitMQ(conf config.Config) (queue.MessageQueuer, error) {
	url := fmt.Sprintf("amqp://guest:guest@%s:%s/", conf.MQ.Host, conf.MQ.Port)
	conn, err := amqp.Dial(url)
	if err != nil {
		log.Printf("error to connection RabbitMQ: %v", err)
		return nil, err
	}
	mq, err := rabbitmq.NewRabbitMQ(conn)
	if err != nil {
		log.Printf("error to new rabbbitMQ: %v", err)
		return nil, err
	}

	return mq, nil
}
