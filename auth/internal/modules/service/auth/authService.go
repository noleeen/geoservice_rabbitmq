package auth

import (
	"auth/internal/entities"
	"auth/internal/errNum"
	"auth/internal/modules/service/userGRPC"
	"context"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/jwtauth"

	"golang.org/x/crypto/bcrypt"
	"time"
)

type AuthService struct {
	UserService userGRPC.UserClienter
	tokenAuth   *jwtauth.JWTAuth
}

func NewAuthService(userService userGRPC.UserClienter, token *jwtauth.JWTAuth) AuthServicer {
	return &AuthService{
		UserService: userService,
		tokenAuth:   token,
	}
}

func (a *AuthService) Register(ctx context.Context, in authEntity.RegisterRequest) authEntity.RegisterResponse {
	hashPass, err := bcrypt.GenerateFromPassword([]byte(in.Password), bcrypt.DefaultCost)
	if err != nil {
		return authEntity.RegisterResponse{
			ErrorStatus: errNum.ErrorHashPassword,
			Message:     "error_auth | Register | generate hashPass ",
		}
	}

	_, err = a.UserService.Profile(ctx, in.Email)
	if err == nil {
		return authEntity.RegisterResponse{
			ErrorStatus: errNum.ErrorUserAlreadyExists,
			Message:     "error_auth | Register | user already exist ",
		}
	}

	newUser := authEntity.User{
		Name:     in.Name,
		Email:    in.Email,
		Password: string(hashPass),
	}

	err = a.UserService.Create(ctx, newUser)
	if err != nil {
		return authEntity.RegisterResponse{
			ErrorStatus: errNum.InternalError,
			Message:     "error_auth | Register | create new user",
		}
	}

	return authEntity.RegisterResponse{
		ErrorStatus: errNum.NoError,
		Message:     "new user success registered with email:" + in.Email,
	}
}
func (a *AuthService) Login(ctx context.Context, in authEntity.LoginRequest) authEntity.LoginResponse {
	profile, err := a.UserService.Profile(ctx, in.Email)
	if err != nil {
		return authEntity.LoginResponse{
			Message:     "error_auth | Login | user not found",
			ErrorStatus: errNum.ErrorUserNotFound,
		}
	}

	err = bcrypt.CompareHashAndPassword([]byte(profile.Password), []byte(in.Password))
	if err != nil {
		return authEntity.LoginResponse{
			Message:     "error_auth | Login | incorrect password",
			ErrorStatus: errNum.PasswordError,
		}
	}

	_, tokenStr, err := a.tokenAuth.Encode(jwt.MapClaims{
		"name": profile.Name,
		"exp":  time.Now().Add(time.Hour * 24).Unix(),
	})
	if err != nil {
		return authEntity.LoginResponse{
			Message:     "error_auth | Login | token generation error",
			ErrorStatus: errNum.GenerateTokenError,
		}
	}

	return authEntity.LoginResponse{
		Name:        profile.Name,
		AccessToken: "Bearer " + tokenStr,
		Message:     "user" + profile.Name + " success authorise",
		ErrorStatus: errNum.NoError,
	}

}
