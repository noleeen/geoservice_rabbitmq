package authEntity

type User struct {
	Id       int
	Name     string
	Email    string
	Password string
}

type RegisterRequest struct {
	Name     string
	Email    string
	Password string
}

type RegisterResponse struct {
	ErrorStatus int
	Message     string
}

type LoginRequest struct {
	Name     string
	Email    string
	Password string
}

type LoginResponse struct {
	Name        string
	AccessToken string
	Message     string
	ErrorStatus int
}
