package config

type Config struct {
	ServerRPC ServerRPC
	Auth      Auth
}

func NewConfig() Config {
	return Config{
		Auth:      getAuthConfig(),
		ServerRPC: getServerRPCConfig(),
	}
}
