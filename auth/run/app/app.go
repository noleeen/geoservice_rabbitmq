package run

import (
	"auth/config"
	"auth/grpcAuth"
	"auth/internal/modules/service/auth"
	userGRPC2 "auth/internal/modules/service/userGRPC"
	"auth/server"
	"context"
	"github.com/go-chi/jwtauth"
	pb "gitlab.com/noleeen/protogeo/gen/auth"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"os"
)

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() error
}

// App - структура приложения
type App struct {
	Conf config.Config
	Rpc  server.Serverer
	Sig  chan os.Signal
}

func NewApp(conf config.Config) *App {
	return &App{
		Conf: conf,
		Sig:  make(chan os.Signal, 1),
	}
}

// Run - запуск приложения
func (a *App) Run() error {
	ctx, cancel := context.WithCancel(context.Background())
	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		log.Println("signal interrupt received", sigInt)
		cancel()
		return nil
	})

	// запускаем  сервер
	errGroup.Go(func() error {
		err := a.Rpc.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return err
	}

	return nil

}

func (a *App) Bootstrap(options ...interface{}) Runner {

	token := jwtauth.New("HS256", []byte(a.Conf.Auth.TokenSecret), nil)
	userGRPC := userGRPC2.NewUserClientGRPC(a.Conf)

	authService2 := auth.NewAuthService(userGRPC, token)

	serverGRPC := grpc.NewServer()

	a.Rpc = server.NewServerRPC(a.Conf.ServerRPC, serverGRPC)
	authGRPC := grpcAuth.NewAuthGRPC(authService2)
	pb.RegisterAutherServer(serverGRPC, authGRPC)

	return a
}
