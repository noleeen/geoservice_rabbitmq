package authService

import (
	"context"
	"fmt"
	pb "gitlab.com/noleeen/protogeo/gen/auth"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"log"
	"net/http"
	"proxy/config"
	"proxy/internal/entities/authEntity"
)

type AuthClientGRPC struct {
	client pb.AutherClient
}

func NewAuthClientGRPC(conf config.Config) *AuthClientGRPC {
	clientConn, err := grpc.Dial(fmt.Sprintf("%s:%s", conf.ServerRPC.AuthHost, conf.ServerRPC.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("|auth| grpc.Dial | error init grpc client ", err)
	}

	client := pb.NewAutherClient(clientConn)
	log.Println("grpc authClient init")
	return &AuthClientGRPC{client: client}
}

func (a *AuthClientGRPC) Register(ctx context.Context, in authEntity.RegisterRequest) authEntity.RegisterResponse {
	resp, err := a.client.Register(ctx, &pb.RegisterRequest{
		Name:     in.Name,
		Email:    in.Email,
		Password: in.Password,
	})
	if err != nil {
		switch status.Code(err) {
		case codes.AlreadyExists:
			return authEntity.RegisterResponse{
				ErrorStatus: http.StatusConflict,
				Message:     err.Error(),
			}
		default:
			return authEntity.RegisterResponse{
				ErrorStatus: http.StatusInternalServerError,
				Message:     err.Error(),
			}
		}
	}

	return authEntity.RegisterResponse{
		ErrorStatus: int(resp.GetStatus()),
		Message:     resp.GetMessage(),
	}

}
func (a *AuthClientGRPC) Login(ctx context.Context, in authEntity.LoginRequest) authEntity.LoginResponse {
	login, err := a.client.Login(ctx, &pb.LoginRequest{
		Email:    in.Email,
		Password: in.Password,
	})
	if err != nil {
		return authEntity.LoginResponse{
			Name:        "error",
			AccessToken: "",
			Message:     login.GetMessage(),
			ErrorStatus: http.StatusInternalServerError,
		}
	}

	return authEntity.LoginResponse{
		Name:        login.Name,
		AccessToken: login.Token,
		Message:     login.Message,
		ErrorStatus: int(login.Status),
	}
}
