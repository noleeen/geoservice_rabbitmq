package userController

import (
	"encoding/json"
	"net/http"
	"proxy/internal/entities/userEntity"
	"proxy/internal/modules/user/userService"
	"proxy/responder"
)

type UserController struct {
	service userService.UserServicer
	respond responder.Responder
}

func NewUserController(service userService.UserServicer, respond responder.Responder) UserControllerer {
	return &UserController{service: service, respond: respond}
}

func (uc *UserController) Profile(w http.ResponseWriter, r *http.Request) {
	var request userEntity.ProfileRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		uc.respond.ErrorBadRequest(w, err)
		return
	}
	profile, err := uc.service.Profile(r.Context(), request.Email)
	if err != nil {
		uc.respond.ErrorInternal(w, err)
		return
	}
	uc.respond.OutputJSON(w, profile)
}
func (uc *UserController) List(w http.ResponseWriter, r *http.Request) {
	list, err := uc.service.List(r.Context())
	if err != nil {
		uc.respond.ErrorInternal(w, err)
		return
	}
	uc.respond.OutputJSON(w, list)
}
