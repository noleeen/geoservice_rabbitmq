package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth"
	"golang.org/x/sync/errgroup"
	"log"
	"net/http"
	"os"
	"proxy/config"
	"proxy/internal/modules"
	"proxy/responder"
	"proxy/router"
	"proxy/server/serverHttp"
)

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() error
}

// App - структура приложения
type App struct {
	Conf   config.Config
	Server serverHttp.Serverer
	Sig    chan os.Signal
}

func NewApp(conf config.Config) *App {
	return &App{
		Conf: conf,
		Sig:  make(chan os.Signal, 1),
	}
}

// Run - запуск приложения
func (a *App) Run() error {
	ctx, cancel := context.WithCancel(context.Background())
	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		log.Println("signal interrupt received", sigInt)
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.Server.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return err
	}

	return nil

}

func (a *App) Bootstrap(options ...interface{}) Runner {

	token := jwtauth.New("HS256", []byte(a.Conf.Auth.TokenSecret), nil)
	chiRouter := chi.NewRouter()
	resp := responder.NewResponder()

	services := modules.NewServices(a.Conf)
	controllers := modules.NewControllers(*services, resp)

	r := router.NewApiRouter(chiRouter, controllers, token)

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.Conf.ServerHttp.Port),
		Handler: r,
	}

	// инициализация сервера
	a.Server = serverHttp.NewHttpServer(srv)

	return a
}
