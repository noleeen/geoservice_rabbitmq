package mq

import (
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/ptflp/gopubsub/queue"
	"gitlab.com/ptflp/gopubsub/rabbitmq"
	"log"
	"os"
)

func RabbitMQ() (queue.MessageQueuer, error) {
	url := fmt.Sprintf("amqp://guest:guest@%s:%s/", os.Getenv("MQ_HOST"), os.Getenv("MQ_PORT"))
	conn, err := amqp.Dial(url)
	if err != nil {
		log.Printf("error to connection RabbitMQ: %v", err)
		return nil, err
	}
	mq, err := rabbitmq.NewRabbitMQ(conn)
	if err != nil {
		log.Printf("error to new rabbbitMQ: %v", err)
		return nil, err
	}

	if err = rabbitmq.CreateExchange(conn, os.Getenv("EXCHANGE_NAME"), os.Getenv("EXCHANGE_TYPE")); err != nil {
		log.Printf("error to create exchange: %v", err)
		return nil, err
	}

	return mq, nil
}
