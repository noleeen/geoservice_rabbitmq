package config

import "os"

type MessageQueuer struct {
	Port         string
	Host         string
	ExchangeName string
	ExchangeType string
}

func getMessageQueuerConfig() MessageQueuer {
	d := MessageQueuer{}
	d.Port = os.Getenv("RABBIT_PORT")
	d.Host = os.Getenv("RABBIT_HOST")
	d.ExchangeName = os.Getenv("EXCHANGE_NAME")
	d.ExchangeType = os.Getenv("EXCHANGE_TYPE")

	return d
}
