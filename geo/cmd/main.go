package main

import (
	"geo/config"
	"geo/run/app"

	"log"
	"os"
)

func main() {
	conf := config.NewConfig()

	app := run.NewApp(conf)

	if err := app.Bootstrap().Run(); err != nil {
		log.Println("|main_Geo| app run error:", err)
		os.Exit(2)
	}
}
