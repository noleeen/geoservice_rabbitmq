package storage

import (
	"geo/internal/entities/geoEntity"
)

type GeoStorager interface {
	AddAddressQueryLink(query, lat, lon string) error
	FindWithLevenshtein(query string) (*geoEntity.Address, error)
}
